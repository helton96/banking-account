CREATE TABLE IF NOT EXISTS transfer(
    id SERIAL PRIMARY KEY,
    sender_document VARCHAR(255) NOT NULL,
    receiver_document VARCHAR(255) NOT NULL,
    value DECIMAL NOT NULL CHECK (value >= 0),
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE INDEX transfer_sender_document_idx ON transfer (sender_document);
CREATE INDEX transfer_receiver_document_idx ON transfer (receiver_document);
CREATE INDEX transfer_created_at_idx ON transfer (created_at);
