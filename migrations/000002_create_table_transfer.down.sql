DROP INDEX IF EXISTS transfer_sender_document_idx;
DROP INDEX IF EXISTS transfer_receiver_document_idx;
DROP INDEX IF EXISTS transfer_created_at_idx;

DROP TABLE IF EXISTS transfer;
