FROM python:3.8-slim AS builder

RUN apt-get update \
  && apt-get install -y \
    libpq-dev \
    build-essential \
  && pip install --upgrade pip  \
  && pip install pipenv

COPY . /app
WORKDIR /app

RUN pipenv lock -r > requirements.txt \
  && pip install -r requirements.txt


CMD ["uvicorn", "app.main:app", "--reload", "--host", "0.0.0.0"]