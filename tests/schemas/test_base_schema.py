from datetime import datetime
from unittest.case import TestCase

from app.api.schemas.base_schema import convert_datetime_iso, convert_field_from_snake_case


class ValidatorBaseSchema(TestCase):
    def test_should_convert_datetime_to_iso_format(self):
        dt = datetime.fromisoformat('2021-09-01T01:11:12.334120+00:00')
        self.assertEqual(convert_datetime_iso(dt), '2021-09-01T01:11:12.334120Z', 'Assert datetime to iso format.')

    def test_should_convert_field_from_snake_case(self):
        fields = ['test_a', 'test_abcd_ef', 'test', 'test_a', 'test_a_b', 'test-a', 'test-Abc-Def-ab']
        expected_field = ['test-a', 'test-abcd-ef', 'test', 'test-a', 'test-a-b', 'test-a', 'test-Abc-Def-ab']
        fields_to_test = [convert_field_from_snake_case(field) for field in fields]
        self.assertEqual(expected_field, fields_to_test, 'Assert convert from snake case.')
