from random import randint
from unittest.case import TestCase
from faker import Faker
from pydantic import ValidationError

from app.api.schemas.transfer.create_transfer import CreateTransferRequest
from tests.utils_test import create_transfer_dict

fake = Faker()


class ValidatorCreateTransferSchema(TestCase):
    def assert_raises(self, data: dict, msg):
        transfer = create_transfer_dict(sender_document='123.456.789-10',
                                        receiver_document='123.456.789-11',
                                        value=randint(1, 500))
        transfer.update(**data)
        self.assertRaises(ValidationError, CreateTransferRequest, **transfer, msg=msg)

    def test_should_parse_to_transfer_schema(self):
        sender_document = '123.456.789-11'
        receiver_document = '123.456.789-11'
        value = randint(1, 500)
        transfer = create_transfer_dict(sender_document, receiver_document, value)
        expected_transfer = {
            'sender_document': sender_document,
            'receiver_document': receiver_document,
            'value': value,
        }
        self.assertDictEqual(CreateTransferRequest(**transfer).dict(), expected_transfer,
                             'Should parse a valid transfer.')

    def test_should_not_parse_to_accounts_schema(self):
        self.assert_raises({'sender-document': ''}, msg='Should not parse with empty sender document')
        self.assert_raises({'sender-document': '12345678911'}, msg='Should not parse with unformatted sender document')
        self.assert_raises({'sender-document': '123.abc.123-11'}, msg='Should not parse with invalid sender document')
        self.assert_raises({'sender-document': '123'}, msg='Should not parse with invalid document')
        self.assert_raises({'sender-document': '123.456.789-111'}, msg='Should not parse with invalid sender document')
        self.assert_raises({'receiver-document': ''}, msg='Should not parse with empty receiver document')
        self.assert_raises({'receiver-document': '12345678911'},
                           msg='Should not parse with unformatted receiver document')
        self.assert_raises({'receiver-document': '123.abc.123-11'},
                           msg='Should not parse with invalid receiver document')
        self.assert_raises({'receiver-document': '123'}, msg='Should not parse with invalid receiver document')
        self.assert_raises({'receiver-document': '123.abc.123-111'},
                           msg='Should not parse with invalid receiver document')
        self.assert_raises({'value': -1}, msg='Should not parse with negative value')
        self.assert_raises({'value': 0}, msg='Should not parse 0')
