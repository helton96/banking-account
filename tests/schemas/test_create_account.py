from random import randint
from unittest.case import TestCase
from faker import Faker
from pydantic import ValidationError

from app.api.schemas.account.create_account import CreateAccountRequest
from tests.utils_test import create_account_dict

fake = Faker()


class ValidatorCreateAccountSchema(TestCase):
    def assert_raises(self, data: dict, msg):
        account = create_account_dict(document='123.456.789-11',
                                      name=fake.pystr(max_chars=255),
                                      available_value=randint(1, 500))
        account.update(**data)
        self.assertRaises(ValidationError, CreateAccountRequest, **account, msg=msg)

    def test_should_parse_to_accounts_schema(self):
        name = fake.pystr(max_chars=255)
        document = '123.456.789-11'
        available_value = randint(1, 500)
        account = create_account_dict(document, name, available_value)
        expected_account = {
            'name': name,
            'document': document,
            'available_value': available_value,
        }
        self.assertDictEqual(CreateAccountRequest(**account).dict(), expected_account,
                             'Should parse a valid account.')

    def test_should_not_parse_to_accounts_schema(self):
        self.assert_raises({'name': ''}, msg='Should not parse with empty name')
        self.assert_raises({'document': ''}, msg='Should not parse with empty document')
        self.assert_raises({'document': '12345678911'}, msg='Should not parse with unformatted document')
        self.assert_raises({'document': '123.abc.123-11'}, msg='Should not parse with invalid document')
        self.assert_raises({'document': '123'}, msg='Should not parse with invalid document')
        self.assert_raises({'document': '123.456.789-111'}, msg='Should not parse with invalid document')
        self.assert_raises({'available-value': -1}, msg='Should not parse with negative value')
