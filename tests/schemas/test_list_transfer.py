from datetime import timedelta, datetime
from random import randint
from unittest.case import TestCase
from faker import Faker
from pydantic import ValidationError

from app.api.schemas.transfer.list_transfer import ListTransferRequestParams
from tests.utils_test import list_transfer_dict

fake = Faker()


class ValidatorListTransferSchema(TestCase):
    def assert_raises(self, data: dict, msg):
        list_transfer = list_transfer_dict(document='123.456.789-11',
                                           create_date_from=datetime.now() - timedelta(days=1),
                                           create_date_to=datetime.now(),
                                           items_quantity=randint(1, 1000),
                                           offset=randint(0, 1000))
        list_transfer.update(**data)
        self.assertRaises(ValidationError, ListTransferRequestParams, **list_transfer, msg=msg)

    def test_should_parse_to_transfer_schema(self):
        document = '123.456.789-11'
        create_date_from = datetime.now().date() - timedelta(days=1)
        create_date_to = datetime.now().date()
        items_quantity = randint(1, 1000)
        offset = randint(0, 1000)
        list_transfer = list_transfer_dict(document, create_date_from, create_date_to, items_quantity, offset)
        expected_transfer = {
            'document': document,
            'create_date_from': create_date_from,
            'create_date_to': create_date_to,
            'items_quantity': items_quantity,
            'offset': offset,
        }
        self.assertDictEqual(ListTransferRequestParams(**list_transfer).dict(), expected_transfer,
                             'Should parse a valid list transfer schema.')

    def test_should_not_parse_to_accounts_schema(self):
        self.assert_raises({'document': '12345678911'}, msg='Should not parse with unformatted document')
        self.assert_raises({'document': '123.456.789.11'}, msg='Should not parse with wrong format document')
        self.assert_raises({'document': ''}, msg='Should not parse with empty document')
        self.assert_raises({'create-date-from': 'wrong-date-a'}, msg='Should not parse with wrong crate date from')
        self.assert_raises({'create-date-to': 'wrong-date-a'}, msg='Should not parse with wrong crate date to')
        self.assert_raises({'items-quantity': 0}, msg='Should not parse with zero items quantity')
        self.assert_raises({'items-quantity': -10}, msg='Should not parse with negative items quantity')
        self.assert_raises({'items-quantity': -1}, msg='Should not parse with negative offset')
