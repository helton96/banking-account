from datetime import datetime, timedelta
from typing import List

import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from starlette.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_422_UNPROCESSABLE_ENTITY, HTTP_200_OK

from tests.utils_test import create_transfer_dict

pytestmark = pytest.mark.asyncio


def assert_list_items(items: List, sender_document: str, receiver_document: str):
    for item in items:
        assert item['sender-document'] == sender_document
        assert item['receiver-document'] == receiver_document


def assert_list_details(data: dict, total_items: int, items_quantity: int, current_page: int, total_pages: int):
    assert data['total-items'] == total_items
    assert data['items-quantity'] == items_quantity
    assert data['current-page'] == current_page
    assert data['total-pages'] == total_pages


async def validate_list_transfer_response(app, client, params, sender_document, receiver_document, pages_detail: dict):
    response = await client.get(app.url_path_for('transfer:list'), params=params)
    data = response.json()
    assert response.status_code == HTTP_200_OK
    assert_list_items(data['items'], sender_document, receiver_document)
    assert_list_details(data, **pages_detail)


async def test_create_transfer_success(
        app: FastAPI, client: AsyncClient, create_accounts_to_transfer,
) -> None:
    first_acc, second_acc = create_accounts_to_transfer
    value_to_transfer = float(first_acc['available_value']) * 0.1
    transfer_json = create_transfer_dict(first_acc['document'], second_acc['document'], value_to_transfer)

    expected_data = {
        'available-value': float(first_acc['available_value']) - value_to_transfer,
        'sender-document': first_acc['document'],
        'receiver-document': second_acc['document']
    }
    response = await client.post(app.url_path_for('transfer:create'), json=transfer_json)
    data = response.json()
    assert response.status_code == HTTP_201_CREATED
    assert set(expected_data.items()).issubset(set(data.items()))


async def test_create_transfer_with_insufficient_value(
        app: FastAPI, client: AsyncClient, create_accounts_to_transfer,
) -> None:
    first_acc, second_acc = create_accounts_to_transfer
    value_to_transfer = float(first_acc['available_value']) + 1
    transfer_json = create_transfer_dict(first_acc['document'], second_acc['document'], value_to_transfer)

    response = await client.post(app.url_path_for('transfer:create'), json=transfer_json)
    data = response.json()
    assert response.status_code == HTTP_400_BAD_REQUEST
    assert data['errors'][0]['sender-document'] == f'{first_acc["document"]} does not have enough money.'
    assert data['message'] == 'SenderDoesNotHaveEnoughMoneyError'


async def test_create_transfer_with_invalid_sender(
        app: FastAPI, client: AsyncClient, create_accounts_to_transfer,
) -> None:
    first_acc, second_acc = create_accounts_to_transfer
    invalid_sender = '123.456.987-11'
    value_to_transfer = float(first_acc['available_value']) * 0.1
    transfer_json = create_transfer_dict(invalid_sender, second_acc['document'], value_to_transfer)

    response = await client.post(app.url_path_for('transfer:create'), json=transfer_json)
    data = response.json()
    assert response.status_code == HTTP_400_BAD_REQUEST
    assert data['errors'][0]['sender-document'] == f'{invalid_sender} does not exists.'
    assert data['message'] == 'AccountDoesNotExistsError'


async def test_create_transfer_with_invalid_receiver(
        app: FastAPI, client: AsyncClient, create_accounts_to_transfer,
) -> None:
    first_acc, _ = create_accounts_to_transfer
    invalid_receiver = '123.456.987-11'
    value_to_transfer = float(first_acc['available_value']) * 0.1
    transfer_json = create_transfer_dict(first_acc['document'], invalid_receiver, value_to_transfer)

    response = await client.post(app.url_path_for('transfer:create'), json=transfer_json)
    data = response.json()
    assert response.status_code == HTTP_400_BAD_REQUEST
    assert data['errors'][0]['receiver-document'] == f'{invalid_receiver} does not exists.'
    assert data['message'] == 'AccountDoesNotExistsError'


async def test_create_transfer_with_invalid_schema(
        app: FastAPI, client: AsyncClient,
) -> None:
    transfer_json = create_transfer_dict('invalid_document', 'invalid_document', -1)
    validation_expected_errors = {'errors': [{'sender-document': 'document must be a valid cpf.'},
                                             {'receiver-document': 'document must be a valid cpf.'},
                                             {'value': 'ensure this value is greater than 0'}],
                                  'message': 'ValidationError'}

    response = await client.post(app.url_path_for('transfer:create'), json=transfer_json)
    data = response.json()
    assert response.status_code == HTTP_422_UNPROCESSABLE_ENTITY
    assert data == validation_expected_errors


async def test_list_transfers_success_with_sender(
        app: FastAPI, client: AsyncClient, create_transfers_between_accounts: List
) -> None:
    transfer = create_transfers_between_accounts[0]
    response = await client.get(app.url_path_for('transfer:list'), params={'document': transfer['sender_document']})
    data = response.json()
    assert response.status_code == HTTP_200_OK
    assert_list_items(data['items'], transfer['sender_document'], transfer['receiver_document'])
    assert_list_details(data, total_items=10, items_quantity=10, current_page=1, total_pages=1)


async def test_list_transfers_success_with_receiver(
        app: FastAPI, client: AsyncClient, create_transfers_between_accounts: List
) -> None:
    transfer = create_transfers_between_accounts[0]

    response = await client.get(app.url_path_for('transfer:list'), params={'document': transfer['receiver_document']})
    data = response.json()
    assert response.status_code == HTTP_200_OK
    assert_list_items(data['items'], transfer['sender_document'], transfer['receiver_document'])
    assert_list_details(data, total_items=10, items_quantity=10, current_page=1, total_pages=1)


async def test_list_transfers_success_with_navigation(
        app: FastAPI, client: AsyncClient, create_transfers_between_accounts: List
) -> None:
    transfer = create_transfers_between_accounts[0]
    params = {'document': transfer['sender_document'], 'items-quantity': 3}
    page_response_detail = {
        'total_items': 10, 'items_quantity': 3, 'current_page': 1, 'total_pages': 4
    }
    await validate_list_transfer_response(app=app, client=client, params=params,
                                          sender_document=transfer['sender_document'],
                                          receiver_document=transfer['receiver_document'],
                                          pages_detail=page_response_detail)

    params['offset'] = 3
    page_response_detail['current_page'] = 2
    await validate_list_transfer_response(app=app, client=client, params=params,
                                          sender_document=transfer['sender_document'],
                                          receiver_document=transfer['receiver_document'],
                                          pages_detail=page_response_detail)

    params['offset'] = 6
    page_response_detail['current_page'] = 3
    await validate_list_transfer_response(app=app, client=client, params=params,
                                          sender_document=transfer['sender_document'],
                                          receiver_document=transfer['receiver_document'],
                                          pages_detail=page_response_detail)

    params['offset'] = 9
    page_response_detail['current_page'] = 4
    page_response_detail['items_quantity'] = 1
    await validate_list_transfer_response(app=app, client=client, params=params,
                                          sender_document=transfer['sender_document'],
                                          receiver_document=transfer['receiver_document'],
                                          pages_detail=page_response_detail)

    params['offset'] = 10
    page_response_detail['current_page'] = 4
    page_response_detail['items_quantity'] = 0
    await validate_list_transfer_response(app=app, client=client, params=params,
                                          sender_document=transfer['sender_document'],
                                          receiver_document=transfer['receiver_document'],
                                          pages_detail=page_response_detail)


async def test_list_transfers_with_not_existent_account(
        app: FastAPI, client: AsyncClient, create_transfers_between_accounts: List
) -> None:
    page_response_detail = {
        'total_items': 0, 'items_quantity': 0, 'current_page': 1, 'total_pages': 1
    }
    await validate_list_transfer_response(app=app, client=client, params={'document': '111.111.111-11'},
                                          sender_document='',
                                          receiver_document='',
                                          pages_detail=page_response_detail)


async def test_list_transfers_with_past_date(
        app: FastAPI, client: AsyncClient, create_transfers_between_accounts: List
) -> None:
    transfer = create_transfers_between_accounts[0]
    page_response_detail = {
        'total_items': 0, 'items_quantity': 0, 'current_page': 1, 'total_pages': 1
    }

    params = {'document': transfer['sender_document'], 'create-date-to': datetime.utcnow().date() - timedelta(days=1)}
    await validate_list_transfer_response(app=app, client=client, params=params, sender_document='',
                                          receiver_document='',
                                          pages_detail=page_response_detail)


async def test_list_transfers_with_future_date(
        app: FastAPI, client: AsyncClient, create_transfers_between_accounts: List
) -> None:
    transfer = create_transfers_between_accounts[0]
    page_response_detail = {
        'total_items': 0, 'items_quantity': 0, 'current_page': 1, 'total_pages': 1
    }

    params = {'document': transfer['sender_document'], 'create-date-to': datetime.utcnow().date() + timedelta(days=2),
              'create-date-from': datetime.utcnow().date() + timedelta(days=1)}
    await validate_list_transfer_response(app=app, client=client, params=params, sender_document='',
                                          receiver_document='',
                                          pages_detail=page_response_detail)


async def test_list_transfers_with_invalid_params(
        app: FastAPI, client: AsyncClient, create_transfers_between_accounts: List
) -> None:
    params = {'document': 'invalid_doc',
              'create-date-to': '2022-02-29',
              'create-date-from': '2022-01-32',
              'offset': '-1', 'items_quantity': 0}

    response = await client.get(app.url_path_for('transfer:list'), params=params)
    expected_data = {
        'errors': [{'document': 'document must be a valid cpf.'},
                   {'create-date-from': 'invalid date format'},
                   {'create-date-to': 'invalid date format'},
                   {'offset': 'ensure this value is greater than or equal to 0'}],
        'message': 'ValidationError'}
    data = response.json()
    assert response.status_code == HTTP_422_UNPROCESSABLE_ENTITY
    assert data == expected_data
