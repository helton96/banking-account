import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from starlette.status import HTTP_201_CREATED, HTTP_422_UNPROCESSABLE_ENTITY

from tests.utils_test import create_account_dict

pytestmark = pytest.mark.asyncio


async def test_create_account_success(
        app: FastAPI, client: AsyncClient,
) -> None:
    account_json = create_account_dict("123.456.789-11", "teste", 1000)
    response = await client.post(app.url_path_for("account:create"), json=account_json)
    data = response.json()
    assert response.status_code == HTTP_201_CREATED
    assert set(account_json.items()).issubset(set(data.items()))


async def test_create_account_with_same_document(
        app: FastAPI, client: AsyncClient,
) -> None:
    account_json = create_account_dict("123.456.789-11", "teste", 1000)
    await client.post(app.url_path_for("account:create"), json=account_json)
    response = await client.post(app.url_path_for("account:create"), json=account_json)
    data = response.json()
    assert response.status_code == HTTP_422_UNPROCESSABLE_ENTITY
    assert data['message'] == "UniqueViolationError"
    assert data['errors'][0]['document'] == f"{account_json['document']} already exists."


async def test_create_account_with_invalid_fields(
        app: FastAPI, client: AsyncClient,
) -> None:
    account_json = create_account_dict("123.456.78911", "", -10)
    validation_expected_errors = {
        'errors': [{'document': 'document must be a valid cpf.'},
                   {'name': 'name should not be empty.'},
                   {'available-value': 'ensure this value is greater than or equal to 0'}],
        'message': 'ValidationError'}

    response = await client.post(app.url_path_for("account:create"), json=account_json)
    data = response.json()
    assert response.status_code == HTTP_422_UNPROCESSABLE_ENTITY
    assert data == validation_expected_errors
