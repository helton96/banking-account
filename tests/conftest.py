from typing import Tuple, List

import pytest
from asgi_lifespan import LifespanManager
from asyncpg.pool import Pool
from fastapi import FastAPI
from httpx import AsyncClient

from app.db.repositories.accounts import AccountsRepository
from app.db.repositories.transfers import TransfersRepository
from tests.fake_asyncpg_pool import FakeAsyncPGPool


@pytest.fixture
def app() -> FastAPI:
    from app.main import get_application
    return get_application()


@pytest.fixture
async def initialized_app(app: FastAPI) -> FastAPI:
    async with LifespanManager(app):
        app.state.pool = await FakeAsyncPGPool.create_pool(app.state.pool)
        # Setup to clear the existent data.
        async with app.state.pool.acquire() as conn:
            await conn.execute('DELETE FROM transfer')
            await conn.execute('DELETE FROM account')
        yield app


@pytest.fixture
def pool(initialized_app: FastAPI) -> Pool:
    return initialized_app.state.pool


@pytest.fixture
async def client(initialized_app: FastAPI) -> AsyncClient:
    async with AsyncClient(
            app=initialized_app,
            base_url="http://testserver",
            headers={"Content-Type": "application/json"},
    ) as client:
        yield client


@pytest.fixture
async def create_accounts_to_transfer(pool: Pool) -> Tuple:
    async with pool.acquire() as conn:
        first_account = await AccountsRepository(conn).create_account(name="test-account_1",
                                                                      document="123.456.789-11",
                                                                      available_value=100)
        second_account = await AccountsRepository(conn).create_account(name="test-account_2",
                                                                       document="123.456.789-12",
                                                                       available_value=100)
        return first_account, second_account


@pytest.fixture
async def create_transfers_between_accounts(pool: Pool, create_accounts_to_transfer: Tuple) -> List:
    first_acc, second_acc = create_accounts_to_transfer
    transfers = []
    async with pool.acquire() as conn:
        for i in range(1, 11):
            value_to_transfer = float(first_acc['available_value']) * 0.01 * i
            transfer = await TransfersRepository(conn).create_transfer(accounts_repo=AccountsRepository(conn),
                                                                       sender_document=first_acc['document'],
                                                                       receiver_document=second_acc['document'],
                                                                       value=value_to_transfer)
            transfers.append(transfer[0])
    return transfers
