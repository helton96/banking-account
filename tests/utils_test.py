from datetime import date


def create_transfer_dict(sender_document: str, receiver_document: str, value: float):
    return {
        'sender-document': sender_document,
        'receiver-document': receiver_document,
        'value': value,
    }


def list_transfer_dict(document: str, create_date_from: date, create_date_to: date, items_quantity: int,
                       offset: int):
    return {
        'document': document,
        'create-date-from': create_date_from,
        'create-date-to': create_date_to,
        'items-quantity': items_quantity,
        'offset': offset,
    }


def create_account_dict(document: str, name: str, available_value: float):
    return {
        "document": document,
        "name": name,
        "available-value": available_value
    }
