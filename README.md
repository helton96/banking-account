# Banking account API

This API is responsible to create accounts and stores its transfers.

#### Setup

Create `.env` file by copying the `.env_template` content.  
The default template runs with docker-compose, if you pretend to run locally, you have to change the database url.


#### Run local App with Docker Compose
Make sure you have docker-compose installed.  
If you don't have, follow the instructions on the link: https://docs.docker.com/compose/install/

```
docker-compose up -d
```
You are ready to go.  
check the docs: http://localhost:8000/docs

#### Run local App
Make sure you have python 3.8 and the database available with its migrations.  
You can use the docker-compose to make the database available:
```
docker-compose up -d postgres
docker-compose up -d migrate
```
Installing the pipenv lib and running the API:
```
pip install pipenv
pipenv lock && pipenv install --dev
pipenv run app
```

#### Run Tests
You must have an instance of the database available to run the routes tests.
```
pipenv run test
```

### Run lint
```
pipenv run flake8 .
```

### Used Libs

**fastAPI**: A high-performance async python web framework used to build the API.  
**uvicorn**: The ASGI server implementation used to spawn the fastAPI framework.  
**asyncpg**: A fast async database interface lib used to connect to the postgres.

### Dev dependencies libs

**faker**: Lib used to generated fake datas for testing.  
**asgi-lifespan**: Lib used to initialize the FastApi for testing the routes.  
**httpx**: Lib used provide async http client for testing the routes.  
**pytest**: Lib used to define the tests.  
**pytest-asyncio**: Lib used to the makes async tests with pytest.  
**flake8**: Lib used to enforce the style guide.

### Web routes
All routes are available on /docs.

### Project structure

    app
    ├── api              - web related stuff.
    │   ├── dependencies - dependencies for routes definition.
    │   ├── schemas      - pydantic schemas used to validate the requests and responses.
    │   └── routes       - web routes.
    ├── db               - db related stuff.
    │   └── repositories - all crud stuff.    
    │   └── queries      - the queries used on repositories.
    ├── errors           - application errors.
    ├── handlers         - application initialization handlers.
    ├── middlewares      - application middlewares.
    ├── services         - logic that is not just crud related.
    └── main.py          - FastAPI application creation and configuration.
    └── settings.py      - application environment.
    migrations           - manually written migrations.
    postman              - postman collection to the defined routes.
    tests                - application tests.

### Project decisions

**How a transfer works?**  
![transfer](readme-imgs/transfer.png)
If there is any error, the transfer will not be committed.  
There is a constraint on the **account available value that checks if it is equal or greater than 0**, 
this will guarantee that the sender will not have less than 0 once a transfer is completed, 
even if there are multiple transfers being sent.


**Transfer history endpoint.**  

**Request:**  
`GET: transfer?document=123.456.789-98&items-quantity=1&create-date-to=2022-01-30&create-date-from=2022-01-01&offset=1`

```
document: required, cpf formatted str.
create-date-from: optional, iso formatted date.
create-date-to: optional, iso formatted date.
items-quantity: optional, 0 < int <= 1000
offset: optional, int > 0
```
All the params have validations.

**Response:**

```
{
    "items": [
        {
            "id": 76,
            "sender-document": "123.456.789-99",
            "receiver-document": "123.456.789-98",
            "value": 11.0,
            "created-at": "2022-01-30T12:18:15.119316Z"
        }
    ],
    "total-items": 3,
    "items-quantity": 1,
    "current-page": 2,
    "total-pages": 3
}
```