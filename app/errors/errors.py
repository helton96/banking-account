from abc import ABC


class Error(Exception, ABC):
    def __init__(self, errors: dict = None, message: str = None, status: int = 500):
        self.status = status
        self.errors = errors
        self.message = message


class ServiceUnavailableError(Error):
    def __init__(self, errors: dict = None, status: int = 500):
        super().__init__(errors=errors, message='ServiceUnavailableError', status=status)


class UniqueViolationError(Error):
    def __init__(self, errors: dict = None, status: int = 422):
        super().__init__(errors=errors, message='UniqueViolationError', status=status)


class AccountDoesNotExistsError(Error):
    def __init__(self, errors: dict = None, status: int = 400):
        super().__init__(errors=errors, message='AccountDoesNotExistsError', status=status)


class SenderDoesNotHaveEnoughMoneyError(Error):
    def __init__(self, errors: dict = None, status: int = 400):
        super().__init__(errors=errors, message='SenderDoesNotHaveEnoughMoneyError', status=status)


class DuplicatedTransferError(Error):
    def __init__(self, errors: dict = None, status: int = 400):
        super().__init__(errors=errors, message='DuplicatedTransferError', status=status)
