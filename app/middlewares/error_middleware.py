import logging
from starlette.requests import Request
from starlette.responses import JSONResponse

logger = logging.getLogger(__name__)


async def catch_exceptions_middleware(request: Request, call_next):
    try:
        return await call_next(request)
    except Exception as e:
        logger.error(f"Error {type(e)} occurred\n")
        logger.exception(e)
        return JSONResponse({"errors": [], "message": "unexpected error has occurred"}, status_code=500)
