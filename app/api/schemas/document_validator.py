import re

CPF_REGEX = r'^(([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}))$'


def is_document_valid(cls, value: str):
    assert re.search(CPF_REGEX, value), 'document must be a valid cpf.'
    return value
