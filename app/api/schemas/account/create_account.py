from pydantic import NonNegativeFloat, validator

from app.api.schemas.base_schema import BaseInputSchema, BaseResponseSchema
from app.api.schemas.document_validator import is_document_valid


class CreateAccountRequest(BaseInputSchema):
    document: str
    name: str
    available_value: NonNegativeFloat

    check_document = validator('document', allow_reuse=True)(is_document_valid)

    @validator('name')
    def is_name_empty(cls, value: str):
        assert len(value) > 0, 'name should not be empty.'
        return value


class CreateAccountResponse(BaseResponseSchema):
    id: int
    name: str
    document: str
    available_value: float
