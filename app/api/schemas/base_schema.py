import datetime

from pydantic import BaseConfig, BaseModel


def convert_field_from_snake_case(string: str) -> str:
    return string.replace("_", "-")


def convert_datetime_iso(dt: datetime.datetime) -> str:
    return dt.replace(tzinfo=datetime.timezone.utc).isoformat().replace("+00:00", "Z")


class BaseResponseSchema(BaseModel):
    class Config(BaseConfig):
        allow_population_by_field_name = True
        alias_generator = convert_field_from_snake_case
        json_encoders = {datetime.datetime: convert_datetime_iso}


class BaseInputSchema(BaseModel):
    class Config(BaseConfig):
        alias_generator = convert_field_from_snake_case
