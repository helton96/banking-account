from typing import List, Optional, Dict

from app.api.schemas.base_schema import BaseResponseSchema


class HttpErrorResponse(BaseResponseSchema):
    errors: Optional[List[Dict]]
    message: str
