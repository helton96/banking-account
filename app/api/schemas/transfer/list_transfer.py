from datetime import datetime, timedelta, date
from typing import Optional, List
from pydantic import Field, NonNegativeInt, validator

from app.api.schemas.base_schema import BaseResponseSchema, BaseInputSchema
from app.api.schemas.document_validator import is_document_valid


class ListTransferRequestParams(BaseInputSchema):
    document: str
    create_date_from: date = datetime.utcnow().date() - timedelta(days=30)
    create_date_to: date = datetime.utcnow().date()
    items_quantity: int = Field(default=10, gt=0, le=1000)
    offset: NonNegativeInt = 0

    check_documents = validator('document', allow_reuse=True)(is_document_valid)


class TransferListData(BaseResponseSchema):
    id: int
    sender_document: str
    receiver_document: str
    value: float
    created_at: datetime


class ListTransferResponse(BaseResponseSchema):
    items: Optional[List[TransferListData]]
    total_items: int
    items_quantity: int
    current_page: int
    total_pages: int
