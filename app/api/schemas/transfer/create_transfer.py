from datetime import datetime as dt

from pydantic import validator, PositiveFloat

from app.api.schemas.base_schema import BaseInputSchema, BaseResponseSchema
from app.api.schemas.document_validator import is_document_valid


class CreateTransferRequest(BaseInputSchema):
    sender_document: str
    receiver_document: str
    value: PositiveFloat

    check_documents = validator('sender_document', 'receiver_document')(is_document_valid)


class CreateTransferResponse(BaseResponseSchema):
    sender_document: str
    receiver_document: str
    available_value: float
    datetime: dt
