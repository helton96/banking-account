from fastapi import APIRouter
from app.api.routes import account, transfer
from app.settings import API_PREFIX

router = APIRouter()

router.include_router(account.router, tags=["account"], prefix=f"/{API_PREFIX}/account")
router.include_router(transfer.router, tags=["transfer"], prefix=f"/{API_PREFIX}/transfer")
