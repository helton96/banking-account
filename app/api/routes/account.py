from fastapi import APIRouter, Depends

from app.api.dependencies.database import get_repository
from app.api.schemas.account.create_account import CreateAccountRequest, CreateAccountResponse
from app.db.repositories.accounts import AccountsRepository

router = APIRouter()


@router.post('', name="account:create", response_model=CreateAccountResponse, status_code=201)
async def create_account(account: CreateAccountRequest,
                         accounts_repo: AccountsRepository = Depends(get_repository(AccountsRepository))):
    created_account = await accounts_repo.create_account(account.name, account.document, account.available_value)
    return created_account
