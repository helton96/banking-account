import math
from datetime import timedelta

from fastapi import APIRouter, Depends, Request

from app.api.dependencies.database import get_repository
from app.api.schemas.transfer.create_transfer import CreateTransferRequest, CreateTransferResponse
from app.api.schemas.transfer.list_transfer import ListTransferRequestParams, ListTransferResponse
from app.db.repositories.transfers import TransfersRepository
from app.db.repositories.accounts import AccountsRepository

router = APIRouter()


@router.post('', name="transfer:create", response_model=CreateTransferResponse, status_code=201)
async def create_transfer(transfer: CreateTransferRequest,
                          transfers_repo: TransfersRepository = Depends(get_repository(TransfersRepository)),
                          accounts_repo: AccountsRepository = Depends(get_repository(AccountsRepository))):
    created_transfer, available_value = await transfers_repo.create_transfer(accounts_repo,
                                                                             transfer.sender_document,
                                                                             transfer.receiver_document,
                                                                             transfer.value)
    return CreateTransferResponse(**created_transfer, **available_value)


@router.get('', name="transfer:list", response_model=ListTransferResponse, status_code=200)
async def list_transfer(request: Request,
                        transfers_repo: TransfersRepository = Depends(get_repository(TransfersRepository))):
    query_data = ListTransferRequestParams(**request.query_params)
    transfers = await transfers_repo.list_transfers(query_data.document,
                                                    query_data.create_date_from,
                                                    query_data.create_date_to + timedelta(days=1),
                                                    query_data.offset,
                                                    query_data.items_quantity)
    total_items = await transfers_repo.count_transfers(query_data.document,
                                                       query_data.create_date_from,
                                                       query_data.create_date_to + timedelta(days=1))
    data = {
        "items": transfers,
        "total_items": total_items,
        "items_quantity": len(transfers),
        "current_page": math.ceil((query_data.offset + 1) / query_data.items_quantity),
        "total_pages": 1 if total_items == 0 else math.ceil(total_items / query_data.items_quantity)
    }
    return data
