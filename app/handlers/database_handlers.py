import logging
import asyncpg
from fastapi import FastAPI

from app import settings

logger = logging.getLogger(__name__)
logging.basicConfig(level=settings.LOGGING_LEVEL)


async def connect_to_db(app: FastAPI) -> None:
    logger.info("Connecting to database.")

    app.state.pool = await asyncpg.create_pool(
        settings.DATABASE_URL,
        min_size=settings.DB_MIN_CONNECTION_SIZE,
        max_size=settings.DB_MAX_CONNECTION_SIZE,
    )

    logger.info("Connection established")


async def close_db_connection(app: FastAPI) -> None:
    logger.info("Closing connection to database")

    await app.state.pool.close()

    logger.info("Connection closed")
