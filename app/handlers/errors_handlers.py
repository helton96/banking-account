from typing import Union

from fastapi.exceptions import RequestValidationError
from pydantic import ValidationError
from starlette.requests import Request
from starlette.responses import JSONResponse

from app.errors.errors import Error
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY


async def error_handler(_: Request, error: Error) -> JSONResponse:
    errors = [] if error.errors is None else [error.errors]
    return JSONResponse({"errors": errors, "message": error.message}, status_code=error.status)


async def validation_error_handler(_: Request, exc: Union[RequestValidationError, ValidationError]) -> JSONResponse:
    errors = [{error['loc'][-1]: error['msg']} for error in exc.errors()]
    return JSONResponse({"errors": errors, "message": "ValidationError"}, status_code=HTTP_422_UNPROCESSABLE_ENTITY)
