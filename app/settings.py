import os
from dotenv import load_dotenv

load_dotenv()

LOGGING_LEVEL = os.getenv('LOGGING_LEVEL', 'INFO')
API_PREFIX = os.getenv('API_PREFIX') or 'api'

DATABASE_URL = os.getenv('DATABASE_URL', 'postgres://postgres:postgres@localhost:5432/banking_account_api')
DB_MAX_CONNECTION_SIZE = int((os.getenv('DB_MAX_CONNECTION_SIZE') or 10))
DB_MIN_CONNECTION_SIZE = int((os.getenv('DB_MIN_CONNECTION_SIZE') or 10))
