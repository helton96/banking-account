import logging
import uvicorn
from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from pydantic import ValidationError

from app.handlers.app_handlers import create_start_app_handler, create_stop_app_handler
from app.middlewares.error_middleware import catch_exceptions_middleware
from app.errors.errors import Error
from app.handlers.errors_handlers import validation_error_handler, error_handler
from app.api.schemas.http.responses import HttpErrorResponse
from app.settings import LOGGING_LEVEL

from app.api.routes.api import router

logger = logging.getLogger(__name__)
logging.basicConfig(level=LOGGING_LEVEL)


def get_application() -> FastAPI:
    application = FastAPI(title='banking_account_api')
    application.middleware('http')(catch_exceptions_middleware)
    application.add_event_handler("startup", create_start_app_handler(application))
    application.add_event_handler("shutdown", create_stop_app_handler(application))
    application.add_exception_handler(Error, error_handler)
    application.add_exception_handler(RequestValidationError, validation_error_handler)
    application.add_exception_handler(ValidationError, validation_error_handler)
    application.include_router(router,
                               responses={422: {'description': 'Validation Error', 'model': HttpErrorResponse},
                                          500: {'description': 'Internal Error', 'model': HttpErrorResponse}})
    return application


app = get_application()

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
