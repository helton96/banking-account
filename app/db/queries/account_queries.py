create_account_query = """
INSERT INTO account (name, document, available_value) VALUES ($1, $2, $3)
RETURNING id, name, document, available_value;
"""

remove_value_from_sender_query = """
UPDATE account
  SET available_value = available_value - $1
  WHERE document = $2
RETURNING available_value;
"""

add_value_to_receiver_query = """
UPDATE account
  SET available_value = available_value + $1
  WHERE document = $2
RETURNING document;
"""