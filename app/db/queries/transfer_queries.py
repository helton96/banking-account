create_transfer_query = """
INSERT INTO transfer (sender_document, receiver_document, value) VALUES ($1, $2, $3)
RETURNING sender_document, receiver_document, created_at as datetime;
"""

verify_duplicated_query = """
SELECT id FROM transfer 
WHERE created_at > NOW() - INTERVAL '2 minutes' 
    AND sender_document = $1 
    AND receiver_document = $2 
    AND value = $3;
"""

get_transactions_query = """
SELECT *
FROM transfer
WHERE (receiver_document = $1 OR sender_document = $1)
    AND created_at BETWEEN $2 AND $3
ORDER BY created_at ASC
OFFSET $4
LIMIT $5;
"""

get_count_transactions_query = """
SELECT COUNT(*)
FROM transfer
WHERE (receiver_document = $1 OR sender_document = $1)
    AND created_at BETWEEN $2 AND $3;
"""
