from datetime import date

from app.db.repositories.accounts import AccountsRepository
from app.db.repositories.base import BaseRepository
from app.db.queries import transfer_queries
from app.errors.errors import DuplicatedTransferError


class TransfersRepository(BaseRepository):
    async def create_transfer(self, accounts_repo: AccountsRepository, sender_document: str, receiver_document: str,
                              value: float):
        if not await self.connection.fetchrow(transfer_queries.verify_duplicated_query,
                                              sender_document,
                                              receiver_document,
                                              value):
            async with self.connection.transaction():
                created_transfer = await self.connection.fetchrow(transfer_queries.create_transfer_query,
                                                                  sender_document,
                                                                  receiver_document,
                                                                  value)
                available_value = await accounts_repo.transfer_value(sender_document, receiver_document, value)
            return created_transfer, available_value

        raise DuplicatedTransferError(errors={})

    async def list_transfers(self, document: str, create_date_from: date, create_date_to: date, offset: int,
                             items_quantity: int):
        transfers = await self.connection.fetch(transfer_queries.get_transactions_query,
                                                document,
                                                create_date_from,
                                                create_date_to,
                                                offset,
                                                items_quantity)
        return transfers

    async def count_transfers(self, document: str, create_date_from: date, create_date_to: date):
        transfers_record_count = await self.connection.fetchrow(transfer_queries.get_count_transactions_query,
                                                                document,
                                                                create_date_from,
                                                                create_date_to)
        return transfers_record_count['count']
