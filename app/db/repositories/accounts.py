from asyncpg import UniqueViolationError as DBUniqueViolationError, CheckViolationError

from app.db.repositories.base import BaseRepository
from app.db.queries import account_queries
from app.errors.errors import UniqueViolationError, AccountDoesNotExistsError, SenderDoesNotHaveEnoughMoneyError


class AccountsRepository(BaseRepository):
    async def create_account(self, name: str, document: str, available_value: float):
        try:
            row = await self.connection.fetchrow(account_queries.create_account_query, name, document, available_value)
            return row
        except DBUniqueViolationError:
            raise UniqueViolationError(errors={"document": f'{document} already exists.'})

    async def transfer_value(self, sender_document: str, receiver_document: str, value: float):
        async with self.connection.transaction():
            try:
                available_value = await self.connection.fetchrow(account_queries.remove_value_from_sender_query,
                                                                 value,
                                                                 sender_document)
            except CheckViolationError:
                error_message = f'{sender_document} does not have enough money.'
                raise SenderDoesNotHaveEnoughMoneyError(errors={"sender-document": error_message})

            if available_value:
                document = await self.connection.fetchrow(account_queries.add_value_to_receiver_query,
                                                          value,
                                                          receiver_document)
                if not document:
                    error_message = f'{receiver_document} does not exists.'
                    raise AccountDoesNotExistsError(errors={"receiver-document": error_message})
                return available_value
            error_message = f'{sender_document} does not exists.'
            raise AccountDoesNotExistsError(errors={"sender-document": error_message})
